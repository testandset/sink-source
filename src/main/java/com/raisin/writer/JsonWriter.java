package com.raisin.writer;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * JSON Writer implementation to POST json objects.
 * 
 * TODO - This needs a interface
 * 
 * @author Deepak.Singh
 *
 */
public class JsonWriter {
	
	private String URL;
	
	public JsonWriter(String url) {
		URL = url;
	}
	
	/**
	 * Handle a response. Currently it only reads 406 status and signals it by passing null.
	 * It needs more work.
	 */
	private ResponseHandler<String> responseHandler = new ResponseHandler<String>() {		
		@Override
		public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
			int status = response.getStatusLine().getStatusCode();
			if(status == 406) {
				return null;
			}
			
			return "OK";
		}
	};
	
	/**
	 * Write to API
	 * @param data
	 * @return
	 */
	public String write(String data) {
		
		// TODO - We should be checking if the data is infact a JSON
		StringEntity requestEntity = new StringEntity(
			    data,
			    ContentType.APPLICATION_JSON);

			HttpPost postMethod = new HttpPost(URL);
			CloseableHttpClient httpclient = HttpClients.createDefault();
			postMethod.setEntity(requestEntity);
			String response = null;

			try {
				response =  httpclient.execute(postMethod, responseHandler);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return response;
	}

}
