package com.raisin;

import com.raisin.reader.JsonReader;
import com.raisin.reader.SourceReader;
import com.raisin.reader.XMLReader;

/**
 * Main app to start processing API.
 * 
 * Creates two threads to start processing A and b end-points.
 * Stars the processing engine to find joins and orphans.
 * 
 * @author Deepak
 *
 */
public class App {
	
	public static void main(String[] args) {
		
		SourceReader readerA = new JsonReader();
		SourceReader readerB = new XMLReader();
		
		readerA.setURL(Constants.SOURCE_A);
		readerB.setURL(Constants.SOURCE_B);
		
		Thread requestToA = new Thread(readerA);
		Thread requestToB = new Thread(readerB);		
		
		// start processing engine first so that queues are nice and warm.
		ProcessingEngine.getInstance().start();		
		
		// Star reading sources
		requestToA.start();
		requestToB.start();		
	}

}
