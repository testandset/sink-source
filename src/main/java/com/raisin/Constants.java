package com.raisin;

public class Constants {

	public static final String SOURCE_A = "http://localhost:7299/source/a";
	
	public static final String SOURCE_B = "http://localhost:7299/source/b";
	
	public static final String SINK_A = "http://localhost:7299/sink/a";
	
}
