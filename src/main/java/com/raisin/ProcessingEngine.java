package com.raisin;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.raisin.writer.JsonWriter;

/**
 * This does all the processing of queues and find out joins and orphans.
 * 
 * Creates a thread for each task and executes them in parallel.
 * 
 * @author Deepak
 *
 */
public class ProcessingEngine  {
	
	private boolean isJoiningFinished = false;
	private boolean isQuiuingFinshedForA = false;
	private boolean isQuiuingFinshedForB = false;	
	
	/**
	 * 	Are all items in a source unique? - A set would be better here to take care of duplicacy.
	 * 
	 * These are unbounded queues and will eat up all memory. Need to bound them. First thing I would
	 * do if I had more time as it will then more logic while adding to queues if they are full and pausing
	 * feeder threads.
	 * 
	 * TODO - Changing of the source queues to HashMap -> O(1) search.
	 * 
	 */
	Queue<String> sourceA = new ConcurrentLinkedQueue<String>();
	Queue<String> sourceB = new ConcurrentLinkedQueue<String>();
	Queue<String> joined = new ConcurrentLinkedQueue<String>();
	Queue<String> orphaned = new ConcurrentLinkedQueue<String>();
	
	// TODO - This needs a interface
	JsonWriter api = new JsonWriter(Constants.SINK_A);
	
	private static ProcessingEngine instance;
	
	/**
	 * Private constructor for singleton
	 */
	private ProcessingEngine() {
		// Singleton
	}
	
	/**
	 * Get an instance of engine
	 * @return
	 */
	public static ProcessingEngine getInstance() {
		if(instance == null) {
			instance = new ProcessingEngine();
		}
		
		return instance;
	}
	
	public void addtoSourceA(String id) {		
		if(id == null) { // poor man's signaling. This could be done better.
			isQuiuingFinshedForA = true;
		} else {
			sourceA.offer(id);
		}
	}
	
	/**
	 * Add to queue B
	 * @param id
	 */
	public void addtoSourceB(String id) {		
		if(id == null) { // poor man's signalling. This could be done better.
			isQuiuingFinshedForB = true;
		} else {
			sourceB.offer(id);
		}
	}

	
	/**
	 * Process queue B
	 */
	public void processSourceA() {
		
		while(true) {
			if(!sourceA.isEmpty()) {
				String a = sourceA.poll();
				processQueue(sourceB, a);
			}
			
			if(sourceA.isEmpty() && isQuiuingFinshedForA) break;
		}
		
		System.out.println("Finished queueing for A");
	}
	
	/**
	 * Process queue B
	 * 
	 * TODO - Code duplicacy, same as previous method - rethink this.
	 */
	public void processSourceB() {
		while(true) {
			if(!sourceB.isEmpty()) {
				String b = sourceB.poll();
				processQueue(sourceA, b);
			}
			
			if(sourceB.isEmpty() && isQuiuingFinshedForB) break;
		}

		System.out.println("Finished queueing for B");
	}
	
	/**
	 * Process a queue to check if the given item is in it.
	 * 
	 * @param queue
	 * @param item
	 */
	private synchronized void processQueue(Queue<String> queue, String item) {
		
		// lazy latch
		if(item == null) {
			return;
		}
		
		if(queue.contains(item)) {
			queue.remove(item);
			joined.offer(item);
		} else {
			if(orphaned.contains(item)) {
				orphaned.remove(item);
				joined.offer(item);				
			} else {
				orphaned.offer(item);
			}
		}
	}
	
	/**
	 * Process all joined entries
	 */
	private void processJoined() {
		while(true) {
			if(!joined.isEmpty()) {
				sink(joined.poll(), "joined");
			}
			
			if(joined.isEmpty() && isQuiuingFinshedForA && isQuiuingFinshedForB) break;
		}
		
		isJoiningFinished = true;
		System.out.println("Finished joining");
	}
	
	/**
	 * Process all orphaned items.
	 * Only start it when the joining has finished as we need to see all elements
	 * in both queues to deduce if an item is orphan.
	 * 
	 * API shuts itself down 6 seconds after all entries have been read, so it only gives us
	 * 6 seconds to finish all orphan processing. This needs bit more thinking.
	 */
	private void processOrphaned() {
		while(true) {
			System.out.print("");
			if(isJoiningFinished && !orphaned.isEmpty()) {
				sink(orphaned.poll(), "orphaned");
			} else if(isJoiningFinished && orphaned.isEmpty()) {
				break;
			}
		}
		
		System.out.println("Finished orphans");		
	}
	
	/**
	 * Post entries to API.
	 * @param id
	 * @param status
	 */
	private void sink(String id, String status) {
		
		while(true) {
			if("OK".equals(api.write("{\"id\": \"" + id + "\",\"kind\": \"" + status + "\"}"))) {
				break;
			};
			
			// If we are here it could mean that API has given 406 response. So wait for bit before retrying.
			try {
				Thread.sleep(100);
				sink(id, status);  // TODO - this could go in a endless loop
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		
	}
	
	/**
	 * Start the juice. Create a thread for each queue and starts processing them
	 */
	public void start() {
		Runnable processA = () -> processSourceA();
		Runnable processB = () -> processSourceB();
		Runnable processSinkForJoined = () -> processJoined();
		Runnable processSinkForOrphaned = () -> processOrphaned();

		Thread processorA = new Thread(processA);
		Thread processorB = new Thread(processB);
		Thread joinProcessor = new Thread(processSinkForJoined);
		Thread orphanedProcessor = new Thread(processSinkForOrphaned);
		
		processorA.start();
		processorB.start();
		joinProcessor.start();		
		orphanedProcessor.start();		
	}

}
