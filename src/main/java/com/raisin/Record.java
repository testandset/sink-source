package com.raisin;

/**
 * Class to hold the an item. Would be useful for doing marshalleling and unmarshalling to json/xml.
 * 
 * @author Deepak
 *
 */
public class Record {
	
	private String status;
	
	private String id;

	public Record(String id, String status) {
		this.id = id;
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
