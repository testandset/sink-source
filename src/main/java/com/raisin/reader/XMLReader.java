package com.raisin.reader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.raisin.ProcessingEngine;

/**
 * XML reader implementation of SourceReader. 
 * Used to read XML endpoints.
 * 
 * @author Deepak
 *
 */
public class XMLReader extends SourceReader {

	Pattern pattern = Pattern.compile("([a-zA-Z0-9]+)=\"(.*?)(?<!\\\\)\"");
	
	/**
	 * Again a very naive implementation to parse XML
	 * TODO - Use a library to parse.
	 * 
	 * This does not validate if the response is a valid XML
	 */
	@Override
	public String parseResponse(String response) {
		Matcher matcher = pattern.matcher(response);
		
		if(response.contains("done")) {
			this.isAllProcessed = true;
			return null;
		}
		
		while(matcher.find())
	    {
	        if(matcher.group(1).equals("value")) {
	        	return matcher.group(2);
	        }
	    }
		
		return null;
	}

	@Override
	public void postProcess(String response) {
		ProcessingEngine.getInstance().addtoSourceB(response);		
	}
	
	// test
	public static void main(String[] args) {
		String res1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><msg><id value=\"626893a90cce8e8ca61152e6388a01c7\"/></msg>";
		String res2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><msg><done/></msg>";
		
		XMLReader reader = new XMLReader();
		
		System.out.println(reader.parseResponse(res1).equals("626893a90cce8e8ca61152e6388a01c7"));
		System.out.println(reader.parseResponse(res2).equals("Done"));
	}

}
