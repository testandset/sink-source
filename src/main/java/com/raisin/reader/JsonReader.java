package com.raisin.reader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.raisin.ProcessingEngine;

/**
 * JSON reader implementation of SourceReader.
 * 
 * Parases responses from JSON endpoints.
 * 
 * @author Deepak
 *
 */
public class JsonReader extends SourceReader  {
	
	Pattern p = Pattern.compile(".*id\":\\s*\"(.+)\"");
	
	/**
	 * Very naive implementation of json parse. 
	 * TODO - Update with JSON library
	 * 
	 * This does not validate if the response is a valid JSON.
	 */
	@Override
	public String parseResponse(String response) {
		
		if (response.contains("done")) {
			this.isAllProcessed = true;
			return null;		
		}

		Matcher matcher = p.matcher(response);
		
		while (matcher.find()) {
			return matcher.group(1);
		}

		return null;
	}

	@Override
	public void postProcess(String response) {
		ProcessingEngine.getInstance().addtoSourceA(response);		
	}
	
	// test
	public static void main(String[] args) {
		String res1 = "{\"status\": \"ok\", \"id\": \"4cd30e56e763101e51a6d4a17d894a3a\"}";
		String res2 = "{\"status\": \"done\"}";
		
		JsonReader reader = new JsonReader();
		
		System.out.println(reader.parseResponse(res1).equals("4cd30e56e763101e51a6d4a17d894a3a"));
		System.out.println(reader.parseResponse(res2).equals("Done"));
	}


}
