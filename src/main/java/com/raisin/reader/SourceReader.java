package com.raisin.reader;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * Abstract for API readers with two abstract methods.
 * 		- parseResponse - How to parse a response
 * 		- postProcess - What needs doing with the resource.
 * 
 * See concrete implementations 
 * @see com.raisin.reader.JsonReader
 * @see com.raisin.reader.XMLReader
 * 
 * 
 * @author Deepak.Singh
 *
 */
public abstract class SourceReader implements Runnable {

	/**
	 * Used for thread signalling
	 */
	protected boolean isAllProcessed = false;

	/**
	 * URL to read it data from
	 */
	private String URL;

	/**
	 * Response handling from API. Hold a thread for 1000 milliseconds if it responds with 406
	 * status.
	 * 
	 * TODO - Handle other 400 and 500 status codes
	 */
	private ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
		@Override
		public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
			int status = response.getStatusLine().getStatusCode();
			if (status == 200) {
				HttpEntity entity = response.getEntity();
				return entity != null ? EntityUtils.toString(entity) : null;
			} else if (status == 406) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			return "";
		}
	};

	/**
	 * Set url
	 * @param url
	 */
	public void setURL(String url) {
		this.URL = url;
	}

	/**
	 * Abstract method to parse a response. Concrete class to decide how to parse.
	 * 
	 * @param response
	 * @return
	 */
	public abstract String parseResponse(String response);

	/**
	 * Abstract method to process response. Concrete class to decide how to process.
	 * @param response
	 */
	public abstract void postProcess(String response);

	/**
	 * Runnable interface method, to pass to a thread.
	 * Continuously reads a response from API.
	 */
	@Override
	public void run() {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget;
		String response;
		
		try {
			httpget = new HttpGet(URL);	
			
			while(!isAllProcessed){
				response = parseResponse(httpclient.execute(httpget, responseHandler));

				if(response != null) {
					postProcess(response);
				}				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		postProcess(null); // signal end
	}

}
